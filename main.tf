resource "aws_acm_certificate" "default" {
  domain_name       = var.domain_name
  validation_method = "DNS"
}

module "domain_verification_record" {
  source          = "git::git@bitbucket.org:christian_m/aws_route53_resource_record.git?ref=v1.0"
  environment     = var.environment
  allow_overwrite = true

  for_each = {
  for dvo in aws_acm_certificate.default.domain_validation_options : dvo.domain_name => {
    name  = dvo.resource_record_name
    value = dvo.resource_record_value
    type  = dvo.resource_record_type
  }
  }


  zone_id = var.zone_id
  record  = {
    name   = each.value.name
    type   = each.value.type
    ttl    = 60
    values = [each.value.value]
  }
}

resource "aws_acm_certificate_validation" "default" {
  certificate_arn         = aws_acm_certificate.default.arn
  validation_record_fqdns = [for record in aws_acm_certificate.default.domain_validation_options : record.resource_record_name]
}
