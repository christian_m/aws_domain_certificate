variable "environment" {
  description = "environment where this resource is used"
  type        = string
}

variable "domain_name" {
  description = "name of the domain for which the certificate should be issued"
  type        = string
}

variable "zone_id" {
  description = "id of the domain zone"
  type        = string
}